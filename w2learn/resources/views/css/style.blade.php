<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Nunito&display=swap" rel="stylesheet">

<!-- Bootstrap CSS File -->
<link href="{{ asset('assets/blog/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

<!-- Libraries CSS Files -->
<link href="{{ asset('assets/blog/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/blog/lib/animate/animate.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/blog/lib/ionicons/css/ionicons.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/blog/lib/owlcarousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/blog/lib/lightbox/css/lightbox.min.css') }}" rel="stylesheet">

<!-- Main Stylesheet File -->
<link href="{{ asset('assets/blog/css/post.css') }}" rel="stylesheet">
<link href="{{ asset('assets/blog/css/style.css') }}" rel="stylesheet">

<style>
  #header .logo img {
    max-height: 40px;
    margin-top: 0;
  }

  #intro .intro-info h2,
  #intro .intro-info p,
  #intro .intro-info .btn,
  .section-header h3 {
    font-family: 'Nunito', sans-serif;
  }
  #intro .intro-info p {
    font-size: 1.2em;
    word-spacing: 1px;
    letter-spacing: 1px;
  }
  .card {
    box-shadow: 0 10px 29px 0 rgba(68, 88, 144, 0.1);
  }
  .mobile-nav {
    overflow-y: scroll;
    background-color: #fff;
    box-shadow: 0px 30px 30px rgba(0, 0, 0, .6);
  }
  .main-nav a,
  .main-nav .drop-down ul a {
    color: #343A40;
  }
  .mobile-nav li {
    padding-left: 15px;
  }
  .mobile-nav a {
    color: #343A40;
    font-size: 14px;
    letter-spacing: 1.2px;
  }
  .mobile-nav-overly {
    background-color: rgba(0, 0, 0, 0.6);
  }
  .btn {
    border-radius: 3px;
  }
</style>