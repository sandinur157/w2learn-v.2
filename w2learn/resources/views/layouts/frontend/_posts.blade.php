<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>W2Learn</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="{{ asset('assets/blog/img/favicon.png') }}" rel="icon">
  <link href="{{ asset('assets/blog/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  @include('css.style')
  <style>
    .post-single .post-body p img {
        width: 100%;
    } 
    .post-single .post-body ul{ margin-left: 2em; margin-bottom: 1em;}
    .post-single .post-body .box { padding-left: 2em;}
    .post-single pre {
        display: block;
        padding: 9.5px;
        margin: 0 0 10px;
        font-size: 13px;
        line-height: 1.42857143;
        color: #333;
        background-color: #f5f5f5;
        word-break: break-all;
        word-wrap: break-word;
        border: 1px solid #ccc;
        border-radius: 4px;
    }
    
    .post-single div.border-content {
        display: block;
        padding: 9.5px;
        margin: 0 0 10px;
        line-height: 1.42857143;
        color: #333;
        word-break: break-all;
        word-wrap: break-word;
        border: 1px solid #ccc;
        border-radius: 4px;
    }
  </style>
    
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
         (adsbygoogle = window.adsbygoogle || []).push({
              google_ad_client: "ca-pub-9070824081326686",
              enable_page_level_ads: true
         });
    </script>
</head>
<body>
  <nav class="navbar navbar-expand-lg d-none">
    <div class="search-area">
      <div class="search-area-inner d-flex align-items-center justify-content-center">
        <div class="close-btn"><i class="fa fa-times" style="font-size: 1.2em;"></i></div>
        <div class="row d-flex justify-content-center">
          <div class="col-md-8">
            <form action="{{ route('search_posts_sidebar') }}">
              <div class="form-group">
                <input type="text" name="query" id="query" placeholder="What are you looking for?">
                <button type="submit" class="submit"><i class="fa fa-search"></i></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </nav>
  <header id="header" class="fixed-top">
    <div class="container">
      <div class="logo float-left"> 
        <a href="{{ url('/') }}" class="scrollto"><img src="{{ asset('assets/blog/img/logo.png') }}" alt="" class="img-fluid"></a>
      </div>

      <nav class="main-nav float-right d-none d-lg-block">
        <ul>
          <li><a href="{{ url('/') }}">Home</a></li>
          <li class="active"><a href="{{ url('/posts') }}">Blog</a></li>
          <li><a href="{{ url('/') }}#portfolio1">Template</a></li>
          <li class="drop-down"><a href="#">Informasi</a>
            <ul>
              <li><a href="#">Tentang Kami</a></li>
              <li><a href="#">Ketentuan</a></li>
              <li><a href="#">Kebijakan</a></li>
            </ul>
          </li>
          <li><a href="{{ url('/contact') }}">Contact Us</a></li>
          @guest
          <li class="drop-down"><a href="#">Mores</a>
            <ul>
              <li><a href="{{ url('/register') }}"><i class="fa fa-terminal"></i> Register</a></li>
              <li><a href="{{ url('/login') }}"><i class="fa fa-sign-in"></i> Login</a></li>
            </ul>
          </li>          
          @endguest
          <li><a href="#" class="search-btn d-none text-muted d-lg-block"><i class="fa fa-search"></i></a></li>
          </ul>
      </nav><!-- .main-nav -->
      
    </div>
  </header><!-- #header -->

  <main id="main">
    <div class="container">
      <div class="row">
          
        <!-- post -->
        @yield('posts')
        
        <aside class="col-lg-4 mt-5">
          <!-- Widget [Search Bar Widget]-->
          <div class="widget card border-0 search">
            <header>
              <h3 class="h6">Search the blog</h3>
            </header>
            <form action="{{ route('search_posts_sidebar') }}" class="search-form">
              <div class="form-group">
                <input type="text" name="query" placeholder="What are you looking for?">
                <button type="submit" class="submit"><i class="fa fa-search"></i></button>
              </div>
            </form>
          </div>
          <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-format="fluid"
                 data-ad-layout-key="-5m+d1+5s-mf+hy"
                 data-ad-client="ca-pub-9070824081326686"
                 data-ad-slot="5334813540"></ins>
            <script>
                 (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
          <!-- Widget [Latest Posts Widget]        -->
          <div class="widget card border-0 latest-posts">
            <header>
              <h3 class="h6">Latest Posts</h3>
            </header>
            @foreach($randomposts as $randompost)
            <div class="blog-posts">
              <a href="{{ route('post.details', $randompost->slug) }}">
                <div class="item d-flex align-items-center">
                  <div class="image">
                    <img src="{{ Storage::disk('public')->url('post/'.$randompost->image) }}" alt="{{ $randompost->title }}" class="img-fluid rounded">
                  </div>
                  <div class="title">
                    <strong>{{ $randompost->title }}</strong>
                    <div class="d-flex align-items-center">
                      <div class="views"><i class="fa fa-eye"></i> {{ $randompost->view_count }}</div>
                      <div class="comments"><i class="fa fa-comments-o"></i>{{ $randompost->comments->count() }}</div>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            @endforeach
          </div>
          <!-- Widget [Categories Widget]-->
          <div class="widget card border-0 categories">
            <header>
              <h3 class="h6">Categories</h3>
            </header>
            @foreach($categories as $category)
                <div class="item d-flex rounded justify-content-between"><a href="{{ route('category.posts',$category->slug) }}">{{ $category->name }}</a><span>{{ $category->count() }}</span></div>
            @endforeach
          </div>
          <!-- Widget [Tags Cloud Widget]-->
          <div class="widget card border-0 tags">       
            <header>
              <h3 class="h6">Tags</h3>
            </header>
            <ul class="list-inline">
              @foreach ($tags as $tag)
                <li class="list-inline-item"><a href="{{ route('tag.posts',$tag->slug) }}" class="tag">#{{ $tag->name }}</a></li>
              @endforeach
            </ul>
          </div>
          <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-format="fluid"
                 data-ad-layout-key="-5m+d1+5s-mf+hy"
                 data-ad-client="ca-pub-9070824081326686"
                 data-ad-slot="5334813540"></ins>
            <script>
                 (adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </aside>
      
      </div>
    </div>
  
  </main>

  <footer id="footer" style="margin-top: -7.2em;">
    @include('layouts.frontend._footer')
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <!-- Uncomment below i you want to use a preloader -->
  <!-- <div id="preloader"></div> -->

  <script src="{{ asset('assets/blog/lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/jquery/jquery-migrate.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/easing/easing.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/mobile-nav/mobile-nav.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/wow/wow.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/waypoints/waypoints.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/counterup/counterup.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/owlcarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/isotope/isotope.pkgd.min.js') }}"></script>
  <script src="{{ asset('assets/blog/lib/lightbox/js/lightbox.min.js') }}"></script>
  <!-- Contact Form JavaScript File -->
  <script src="{{ asset('assets/blog/contactform/contactform.js') }}"></script>
  <script src="{{ asset('assets/frontend/js/tether.min.js') }}"></script>

  <!-- Template Main Javascript File -->
  <script src="{{ asset('assets/blog/js/main.js') }}"></script>
  <script src="{{ asset('assets/frontend/js/swiper.js') }}"></script>
  <script src="{{ asset('assets/frontend/js/scripts.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>
  {!! Toastr::message() !!}
  <script>

    $('.search-btn').on('click', function (e) {
        e.preventDefault();
        $('.navbar-expand-lg').removeClass('d-none')
        $('.search-area').fadeIn();
        $('#search').focus()
    });
    $('.search-area .close-btn').on('click', function () {
        $('.navbar-expand-lg').addClass('d-none')
        $('.search-area').fadeOut();
    });

      @if($errors->any())
      @foreach($errors->all() as $error)
      toastr.error('{{ $error }}','Error',{
          closeButton:true,
          progressBar:true,
      });
      @endforeach
      @endif
  </script>
  @stack('js')

</body>
</html>
