<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $query = $request->input('query');
        $posts = Post::where('title','LIKE',"%$query%")->approved()->published()->get();
        return view('search',compact('posts','query'));
    }

    public function search_posts(Request $request)
    {
        $query = $request->input('query');
        $posts = Post::where('title','LIKE',"%$query%")->approved()->published()->get();
        return view('search_posts', compact('posts','query'));
    }

    public function search_posts_sidebar(Request $request)
    {
        $randomposts = Post::approved()->published()->take(6)->inRandomOrder()->get();
        $categories  = Category::all();
        $tags        = Tag::all();

        $query = $request->input('query');
        $posts = Post::where('title','LIKE',"%$query%")->approved()->published()->get();
        return view('search_posts_sidebar', compact('posts','query',  'randomposts', 'categories', 'tags'));
    }
}
