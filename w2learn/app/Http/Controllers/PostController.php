<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PostController extends Controller
{

    public function index()
    {
        $randomposts = Post::approved()->published()->take(6)->inRandomOrder()->get();
        $categories  = Category::all();
        $tags        = Tag::all();

        $posts = Post::latest()->approved()->published()->paginate(10);
        return view('posts', compact('posts', 'randomposts', 'categories', 'tags'));
    }
    
    public function details($slug)
    {
        $randomposts = Post::approved()->published()->take(6)->inRandomOrder()->get();
        $categories  = Category::all();
        $tags        = Tag::all();
        $paginates   = Post::approved()->published()->take(2)->inRandomOrder()->get();
        $post = Post::where('slug', $slug)->approved()->published()->first();        

        $blogKey = 'blog_' . $post->id;

        if (!Session::has($blogKey)) {
            $post->increment('view_count');
            Session::put($blogKey, 1);
        }

        return view('post', compact('post', 'randomposts', 'categories', 'tags', 'paginates'));
    }

    public function postByCategory($slug)
    {
        $randomposts = Post::approved()->published()->take(6)->inRandomOrder()->get();
        $categories  = Category::all();
        $tags        = Tag::all();

        $category = Category::where('slug', $slug)->first();
        $posts = $category->posts()->approved()->published()->get();
        return view('category', compact('category', 'posts', 'categories', 'randomposts', 'tags'));
    }

    public function postByTag($slug)
    {
        $randomposts = Post::approved()->published()->take(6)->inRandomOrder()->get();
        $categories  = Category::all();
        $tags        = Tag::all();

        $tag = Tag::where('slug', $slug)->first();
        $posts = $tag->posts()->approved()->published()->get();
        return view('tag', compact('tag', 'posts', 'categories', 'randomposts', 'tags'));
    }
}
